#!/usr/bin/python3

# 1. Supposons que msg = 'Hello World'. Que se passe-t-il si vous demandez à l'interpréteur d'évaluer   msg[ : : -1]? pourquoi ce résultat est-il raisonnable ?

#Reponses:
## Le message sera inversé: 'dlrow olleH'
## on a demandé d'afficher tout le message sans condition, ni en spécifiant le pas, en commençant par la dernière case. 
#=========================================#

# 2. Traitez la liste chomsky en utilisant une boucle for, et stockez le résultat dans une nouvelle liste nommée longueurs de telle sorte que cette dernière contiendra les longueurs des mots contenus dans la liste chomsky. utilisez append ()

chomsky = ['colorless', 'green', 'ideas', 'sleep', 'furiously']
longueurs = []
for element in chomsky:
    longueurs.append(len(element))

# Définissez une variable silly qui contient la chaîne: ’newly formed bland ideas are inexpressible in an infuriating way’. Ecrivez un programme pour effectuer les tâches suivantes:
silly = 'newly formed bland ideas are inexpressible in an infuriating way'

# a) Diviser silly en une liste de chaînes, une pour chaque mot de silly, en utilisant l’opération split() de Python, et sauvegarder le résultat dans une variable appelée listeSilly.
listeSilly = silly.split()

# b) Extraire la deuxième lettre de chaque mot de listeSilly et les joindre dans une chaîne, pour obtenir ’eoldrnnnna’.
eoldrnnnna = ''
for element in listeSilly:
    eoldrnnnna += element[1]

# c) Recombiner les mots de listeSilly de telle sorte à obtenir une seule chaîne de caractères, en utilisant join (). Assurez-vous que les mots de la chaîne résultante sont séparés par des espaces.
newListeSilly = ' '.join(listeSilly)

# d) Imprimer les mots de silly par ordre alphabétique, un par ligne.
# for element in sorted(listeSilly):
#     print(element)
#=========================================#

# 3. La fonction index() peut être utilisée pour rechercher des éléments dans des séquences. Par exemple, ’inexpressible’.index(’e’) nous retourne l’index de la première occurrence de la lettre ‘e’ dans le mot ’inexpressible’.

# a) Que se passe-t-il lorsque vous recherchez une expression (sous-chaine), comme dans ’inexpressible’. index(’re’)? Reponses: cela affiche l'index du debut de la première occurence de la sous-chaine voulue.
# inexpressible = "inexpressible"
# print(inexpressible.index('re'))

# b) Définir une variable words qui contient une liste de mots. Utilisez maintenant words.index() pour rechercher la position d’un mot quelconque.
words = 'Définir une variable words qui contient une liste de mots Utilisez maintenant words.index() pour rechercher la position d’un mot quelconque'.split()
word = 'position'
# if word in words:
#     print(words.index(word))

# c) Définir une variable silly comme dans l’exercice précédent. Utilisez la fonction index() en la combinant au « slicing » de listes pour construire une phrase qui contient tous les mots jusqu’à (mais sans inclure) le mot ’in’ de silly.
miniSilly = ' '.join(silly.split()[0:silly.split().index('in')])
#=========================================#

# 4. Assignez la valeur ’she sells sea shells by the sea shore’ à la variable sentence puis écrivez un programme pour accomplir ce qui suit:
sentence = 'she sells sea shells by the sea shore'
newSentence = ''
for word in sentence.split():
# a) Imprimer tous les mots qui commencent par ‘sh’
    # if word.startswith('sh'):
    #     print(word)
# b) Imprimer tous les mots qui sont plus longs que 4 caractères.
    # if len(word) > 4:
    #     print(word)
# c) Générer une nouvelle phrase qui contient le mot ‘like’ avant chaque mot de sentence qui commence par ‘se’. Votre résultat doit consister d’une seule chaîne de caractères.
    if word.startswith('se'):
        newSentence += ' like '+word
# print(newSentence)
#=========================================#

# 5. Écrire un programme pour abréger un texte en supprimant toutes ses voyelles.
text = 'Cette année, avec la thématique «Open Science», l’idée sera principalement de répondre à la sous-représentation des non-informaticiens quant au Libre que l\'on considère injustement exclusivement lié à l\'informatique. Nos conférences, pour la première fois, se concentrent sur d\'autres aspects que la présentation frontale de projets Libre Open Source et abordent des concepts beaucoup plus généraux. C\'est donc dans cette optique là qu\'il nous a paru naturel de nous ouvrir d’avantage au monde du libre open source !'

def removeVow(text):
    voyelles = ['a', 'e', 'i', 'o', 'u', 'y']
    word = []
    for letter in text:
        if letter.lower() not in voyelles:
            word.append(letter)
    return "".join(word)

# import re
# def removeVow(text):
#     return re.sub("[aeiouy]", "", text.lower())

# print(removeVow(text))
#=========================================#

# 6. Créez deux dictionnaires, d1 et d2, et ajoutez quelques entrées à chacun. Maintenant lancez la commande d1.update(d2). Que se passe-t-il? Comment ceci pourrait-il être utile?

d1 = {}
d2 = {}
for word in text.split():
    d1[text.split().index(word)] = word
# print(d1)

for word in silly.split():
    d2[silly.split().index(word)] = word

# print(d2)

d1.update(d2)
# print(d1)
#Elle remplace les valeurs des clés du 'd2' qui sont similaires avec 'd1'
#=========================================#

# 7. Ecrire un programme qui prend une phrase exprimée comme une seule chaîne, la divise et compte ses mots. Faites que ce programme imprime chaque mot et sa fréquence, un par ligne, et par ordre alphabétique. Utilisez une ou plusieurs fonctions pour le faire.
from nltk import defaultdict
# from nltk.book import *
def split_count(text):
    return text.split(), len(text.split())

def frequence(text):
    textSplited, textLength = split_count(text)
    textDict = defaultdict(int)

    for word in textSplited:
            # textDict[word] += round(float(1 / textLength * 100), 3)
            textDict[word] += 1
    return textDict

def printSortedDic(dic):
    for item in sorted(dic.items()):
        print(item)

# text = nltk.corpus.gutenberg.words('shakespeare-macbeth.txt')
printSortedDic(frequence(text))

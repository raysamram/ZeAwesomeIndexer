# Python program that can be executed to report whether particular
# python packages are available on the system.

import os
import math
import sys

def test_numpy():
    try:
        import numpy as np
    except ImportError:
        print("Could not import numpy -> numpy failed")
        return None
    # Simple test
    a = np.arange(0, 100, 1)
    assert np.sum(a) == sum(a)
    print("-> numpy OK")


def test_scipy():
    try:
        import scipy
    except ImportError:
        print("Could not import 'scipy' -> scipy failed")
        return None
    # Simple test
    import scipy.integrate
    assert abs(scipy.integrate.quad(lambda x: x * x, 0, 6)[0] - 72.0) < 1e-6
    print("-> scipy OK")


def test_pylab():
    """Actually testing matplotlib, as pylab is part of matplotlib."""
    try:
        import pylab
    except ImportError:
            print("Could not import 'matplotlib/pylab' -> failed")
            return None
    # Creata plot for testing purposes
    xvalues = [i * 0.1 for i in range(100)]
    yvalues = [math.sin(x) for x in xvalues]
    pylab.plot(xvalues, yvalues, "-o", label="sin(x)")
    pylab.legend()
    pylab.xlabel('x')
    testfilename='pylab-testfigure.png'

    # check that file does not exist yet:
    if os.path.exists(testfilename):
        print("Skipping plotting to file as file {} exists already."\
            .format(testfilename))
    else:
        # Write plot to file
        pylab.savefig(testfilename)
        # Then check that file exists
        assert os.path.exists(testfilename)
        print("-> pylab OK")
        os.remove(testfilename)


def test_sympy():
    try:
        import sympy
    except ImportError:
            print("Could not import 'sympy' -> fail")
            return None
    # simple test
    x = sympy.Symbol('x')
    my_f = x ** 2
    assert sympy.diff(my_f,x) == 2 * x
    print("-> sympy OK")


def test_pytest():
    try:
        import pytest
    except ImportError:
            print("Could not import 'pytest' -> fail")
            return None
    print("-> pytest OK")


if __name__ == "__main__":
    print("Running using Python {}".format(sys.version))

    print("Testing numpy...     "),
    test_numpy()

    print("Testing scipy...     "),
    test_scipy()

    print("Testing matplotlib..."),
    test_pylab()

    print("Testing sympy...     "),
    test_sympy()

    print("Testing pytest...    "),
    test_pytest()

#### 
#import nltk
#from nltk.book import *
#
#print(len(text1))
#print(len(set(text1)))
#
#print(sorted(set(text3)) )
#def score(text):
#    return len(text) / len(set(text))
#
#
#print(score(text3))
#
#print(text1)
#text1.concordance("monstrous")
#text4.dispersion_plot(['citizens', 'democracy', 'freedom', 'duties', 'America'])
#len(text1)
#len(set(text1))
#sorted(set(text3)) 
#def score(text):
#    return len(text) / len(set(text))
#
#msg = 'Hello World'
#print (msg[0])
#print (msg[4])
#print (len(msg))
#print (msg[-1])
#
#print (msg[1:4])
#print (msg[:3])
#print (msg[6:])
#print (msg[6:11:2])
#print (msg)
#
#cgi = ['colorless', 'green', 'ideas']
#print (len(cgi))
#print (cgi[0])
#print (cgi[-1])
#print (cgi[1:3])
#print (cgi[-2:])
#chomsky = cgi + ['sleep', 'furiously']
#cgi[0] = 'colorful'
#
##msg[0] = ’J’     ###########  ERROR !!
#print("hhhhhhhhhhhhhhhhhhhhhhhhhhhh")
#print (sorted(chomsky))
#sss=reversed(chomsky)
#print (sss)
#print (chomsky.append('said'))
#print (chomsky.append('Chomsky'))
#print (chomsky.index('green'))
##>>> for num in [1, 2, 3]:
##...          	print ’The number is’, num
##>>> chomsky = [’colorless’, ’green’, ’ideas’, ’sleep’, ’furiously’]
##>>> for word in chomsky:
##        ... 	print len(word), word[-1], word
##>>> total = 0	
##>>> for word in chomsky:
##...          	total += len(word)
##...
##>>> total / len(chomsky)
##>>> sent = ’colorless green ideas sleep furiously’
##>>> for char in sent:
##       ... 	print char,
##>>> for foo123 in ’colorless green ideas sleep furiously’:
##... 	print foo123,
##>>> for foo123 in [’colorless’, ’green’, ’ideas’, ’sleep’, ’furiously’]:
##... 	print foo123
#
#
#def score(text):
#    	 return len(text) / len(set(text))
#
#print(score(text3))
#
#chomsky = ['colorless', 'green', 'ideas', 'sleep', 'furiously']
#print(chomsky.index('ideas'))
#chomsky.sort()
#print(chomsky)
#chomsky.reverse()
#print(chomsky)
#chomsky.append('and')
#print(chomsky)
#    
#sent = "colorless green ideas sleep furiously"
#for char in sent:
#    print (char)
#for char in sent:
#    print (char,end="")
#
#mixed = ['cat', '', ['dog'], []]
#for element in mixed:
#    if element:
#        print (element)          # non-empty string or list is evaluated as true; false otherwise
#print ("done!")
#
#

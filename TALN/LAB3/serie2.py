#!/usr/bin/python3

#1. Écrire des expressions régulières qui représentent les classes de chaînes de caractères suivantes:
import re
### a) Un seul déterminant (en supposant que ‘a’, ‘an’, et ‘the’ sont des déterminants).

text = 'Cette année, avec la thématique «Open Science», (44.4 * 5.66) + (5 /6) l’idée sera principalement de répondre à la sous-représentation des non-informaticiens quant au Libre que l\'on considère injustement exclusivement lié à l\'informatique. Nos 4-8.2 conférences, pour la première fois, se concentrent sur d\'autres aspects que la présentation frontale de 12 /5 projets Libre Open Source et abordent des concepts beaucoup plus généraux. the an the C\'est donc dans cette An optique A là qu\'il nous a paru naturel de nous ouvrir d’avantage au monde du libre The open source !'

determinant = re.findall(r'\b([aA]n?|[tT]he)\b', text)

print(determinant, end='\n===========================================\n')

###b) Une expression arithmétique qui utilise des réels, les 4 opérateurs arithmétiques, et les parenthèses. (Nous supposerons que les expressions entre parenthèses ne sont pas imbriquées) Testez ces expressions régulières pour la recherche et extraction de telles expressions dans une chaine de caractères.

# arithmetic = 5 + 5
# p = re.compile("\(?\d+\.?\d*\s*[-+*/]\s*\d+\.?\d*\)?")
p1 = "\(?\d+\.?\d*\s*[-+*/]\s*\d+\.?\d*\)?"
p2 = "\(?"+p1+"\s?[-*+/]\s?"+p1

arithmetic_search = re.findall(p2, text)

print(arithmetic_search, end='\n===========================================\n')

##############################################################
# 2. L'exemple vu en TP de l'extraction de paires consistant de (nom, domaine) à partir d’un texte ne fonctionne pas lorsque il y a plus d'une adresse e-mail dans une ligne, parce que l'opérateur + est «gourmand» et consomme trop de caractères de l’input.

email = "(ahmedabdellAOui@outlook.fr) étudiant à pas compris ABIDI Souad fs_abidi@esi.dz etudiante à l\'ESI à ESI alger ADDAD Anis anissous1997@gmail.com Etudiant à USTHB AGUEMOUN Mohamad amine amineaguemoun@gmail.com étudiant à étudiant AHDJOUDJ Ines (i.nes@hotmail.com) Etudiante en informatique à USTHB AIT AKKACHE  oufiane <ait.akkache.soufiane@gmail.com> Developpeur à Nomisma ALEM Ismail sam3an2899@gmail.com etudiant Université d'Alger 1 ALIM Yanis yanis15ziani@live.fr Etudiant à usthb ALIOUAT Ahcen ahcen2300 gmail.com etudiant MASTER1 à USTHB AMAROUCH Walid amarouch.walid@gmail.com"

## a) Faites des tests avec un texte en entrée contenant plusieurs adresses e-mail par ligne, tel que celui donné dans la chaine s ci-dessous. Que se passe-t-il?


## b) en utilisant re.findall( ), écrire une autre expression régulière pour extraire des adresses e-mail, en remplaçant le caractère ‘.’ par un intervalle ou une négation d’intervalle, comme [a-z]+ ou [^ >] +
emailRegex = re.findall("[\w!#$%&'*+-/=?^_~\{\|\}]+@[\w!#$%&'*+-/=?^_~\{\|\}]+\.[a-zA-Z]+" , email)
# emailRegex = re.findall(r"<(.+)@(.+)>" , email)
print(emailRegex, end='\n===========================================\n')

# c) Maintenant, essayez de faire correspondre des adresses email en remplaçant l'expression régulière ‘.+’ par son expression équivalente "non-gourmande" ‘.+?’



# s3 = """
# <hart@vmd.cso.uiuc.edu> Ward <Martin.Ward@uk.ac.durham>
# Final editing was done by Martin Ward
# <Martin.Ward@uk.ac.durham>
# Michael S. Hart <hart@pobox.com>
# Prepared by David Price, email <ccx074@coventry.ac.uk>"""

# print(re.findall(r'<(.+)@(.+)>', s3))

# mo = re.search("([0-9]+).*: (.*)", "Customer number: 232454, Date: February 12, 2011")
# print(mo.group())